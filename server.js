var express = require("express");
var app = express();

app.use('/store', function(req, res, next) {
  console.log("I am Your agent in Your request to /store!");
  next();
});

app.get('/', function (req, res) {
    res.send('Hello World!');
})

app.get("/store", function(req, res) {
  res.send("This is store");
});

app.listen(3000);
app.use(function (req, res, next) {
    res.status(404).send('Sorry, but we could not find things, You have requested!')
});
 
